﻿using System;
using System.Threading;
class Program
{
    static void Main(string[] args)
    {
        Inicio_sesion(); // Llamado a menu principal
    }

    static void Menu(string user_name) /// Menu de manejo de ordeenes
    {
        Order_Handler manejaOrdenes = new Order_Handler();
        while (true)
        {

            Console.WriteLine("Seleccione una opción:");
            Console.WriteLine("1. Crear una orden");
            Console.WriteLine("2. Ver órdenes creadas");
            Console.WriteLine("3. Eliminar una orden");
            Console.WriteLine("4. Salir");

            int opcion = int.Parse(Console.ReadLine());

            switch (opcion)  // Escoger opcion de menu
            {
                case 1:
                    manejaOrdenes.create_order(user_name);
                    manejaOrdenes.GuardarInfo("Archivo_ordenes.txt", user_name);
                    break;
                case 2:

                    manejaOrdenes.CargarInfoArchivo("Archivo_ordenes.txt", user_name);
                    break;
                case 3:
                    manejaOrdenes.eliminar_orden(user_name);
                    break;
                case 4:
                    cerrar_sesion(user_name);
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Opción inválida. Intente nuevamente.");
                    break;
            }
        }
    }
    static void Inicio_sesion() /// Menu Principal <agrega usuario o iniciar sesion> 
    {



        while (true)
        {

            Console.WriteLine("Seleccione una opción:");
            Console.WriteLine("1. Agregar Usuario");
            Console.WriteLine("2. Iniciar sesion");
            Console.WriteLine("3. Salir");

            int opcion = int.Parse(Console.ReadLine());

            switch (opcion) // Escoger opcion de menu
            {
                case 1:
                    agregar_usuario();
                    break;
                case 2:
                    autenticacion_usuario();

                    break;

                case 3:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Opción inválida. Intente nuevamente.");
                    break;
            }
        }

    }
    static void agregar_usuario() // Agrega usuario a la BD del servidor
    {
        string user_name;
        string contrasena;
        string cel_number;
        string id_number;
        string separador = "#";
        string option = "A";
        /// Agrega usuario >> pide datos al usuario final
        Console.WriteLine("Bienvenido a la Aplicacion OrderHandler");
        Console.WriteLine("---------------------------------------");
        Console.WriteLine("1. Agregar Usuario");
        user_name = Console.ReadLine();
        Console.WriteLine("---------------------------------------");
        Console.WriteLine("2. Digite su contrasena");
        contrasena = Console.ReadLine();
        Console.WriteLine("---------------------------------------");
        Console.WriteLine("3. Digite su numero de telefono");
        cel_number = Console.ReadLine();
        Console.WriteLine("---------------------------------------");
        Console.WriteLine("4. Digite su numero de cedula");
        id_number = Console.ReadLine();
        /// Fin Agrega usuario >> pide datos al usuario final
        string mensaje = option + separador + id_number + separador + user_name + separador + contrasena + separador + cel_number;  // concatenacion de mensaje hacia servidor

        Console.WriteLine(SocketClient.StartClient(mensaje)); // llamado al socket para enviar audit log





    }

    static string autenticacion_usuario() /// Valida credenciales contra la BD del servidor
    {  /// Ingreso al sistema
        string user_name;
        string contrasena;
        string separador = "#";
        string option = "B";
        string mensaje;
        string validar = "";
        do
        {
            // Solicitud de credenciales
            Console.WriteLine("Bienvenido a la Aplicacion OrderHandler");
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("1. Digite su ID");
            user_name = Console.ReadLine();
            Console.WriteLine("---------------------------------------");
            Console.WriteLine("2. Digite su contrasena");
            contrasena = Console.ReadLine();
            //  FIN Solicitud de credenciales

            mensaje = option + separador + user_name + separador + contrasena; // concatenacion de mensaje hacia servidor
            validar = SocketClient.StartClient(mensaje); // llamado al socket para enviar audit log y recibir respuesta de acceso concedido o denegado
            Console.WriteLine(validar);

        } while (validar != "Inicio de sesion exitoso."); // Fuerza al usuario a ingresar credenciales validas



        if (validar == "Inicio de sesion exitoso.") ///  Autenticacion correcta
        { 
            Menu(user_name); // Llamado a la funcion ordenes
        }
        return user_name;   // retornar username para uso de TAGS (quien creo la orden / vio / elimino ) en otras funciones
    }

    static void cerrar_sesion(string user_name)
    { // cierre de scion
        string option = "C";
        string separador = "#";
        string user111 = user_name;

        string mensaje = option + separador + user111; // concatenacion de mensaje hacia servidor


        Console.WriteLine(SocketClient.StartClient(mensaje)); // llamado al socket para enviar audit log de cierre de sesions
    }
}