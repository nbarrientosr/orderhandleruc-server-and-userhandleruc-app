﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

class Order_Handler
{
    // Listas para guardado de datos en memoria
    List<int> lista_id = new List<int>();
    List<double> lista_precios = new List<double>();
    List<DateTime> lista_fechas_creacion = new List<DateTime>();
    List<string> lista_nombre_articulos = new List<string> { };
    List<string> lista_usuario = new List<string> { };


    public void create_order(string user)  //////// Crear ordenes en RAM
    {
        try
        {
            ////// Toma de datos
            Console.WriteLine("Ingrese el ID de la orden:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese el nombre del artículo:");
            string nombre = Console.ReadLine();
            Console.WriteLine("Ingrese el precio del artículo:");
            double precio = double.Parse(Console.ReadLine());
            DateTime fechaCreacion = DateTime.Now;

            // Agregar elementos a las listas
            lista_id.Add(id);
            lista_nombre_articulos.Add(nombre);
            lista_precios.Add(precio);
            lista_fechas_creacion.Add(fechaCreacion);
            lista_usuario.Add(user);

            Console.WriteLine("Orden creada con éxito.");
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex.Message); /// Manejo de excepciones
        }
    }



    public void eliminar_orden(string user_name) /// Borrado de ordenes basado en order ID y username , parametro permite borrar solo las ordenes creadas por el usuario
    {
        string deletUser = user_name; 
        string opcion = "E";
        string separador = "#";
        try
        {
            if (lista_id.Count == 0)
            {
                Console.WriteLine("No hay ordenes creadas");
            }
            else
            {
                Console.WriteLine("Ingrese el ID de la orden que desea eliminar:");
                int idEliminar = int.Parse(Console.ReadLine());
                int index = lista_id.IndexOf(idEliminar);
                if (index <= -1)
                {
                    Console.WriteLine("La orden no existe.");
                }
                else
                {
                    string mensaje = opcion + separador + deletUser + separador + lista_id[index];  // concatenacion de mensaje hacia servidor
                    lista_id.RemoveAt(index);
                    lista_nombre_articulos.RemoveAt(index);
                    lista_precios.RemoveAt(index);
                    lista_fechas_creacion.RemoveAt(index);
                    lista_usuario.RemoveAt(index); 
                    Console.WriteLine("Orden eliminada con éxito.");

                    SocketClient.StartClient(mensaje); // llamado al socket para enviar audit log

                    // Eliminar la orden del archivo .txt
                    string archivoPath = "Archivo_ordenes.txt"; // Reemplaza "ruta_del_archivo.txt" con la ruta correcta
                    string[] lineasArchivo = File.ReadAllLines(archivoPath);
                    lineasArchivo = lineasArchivo.Where(linea => !linea.Contains(idEliminar.ToString())).ToArray();
                    File.WriteAllLines(archivoPath, lineasArchivo);
                    Console.WriteLine("Orden eliminada del archivo .txt.");
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error al eliminar la orden: " + ex.Message); /// Manejo de excepciones
        }
    }




    public void GuardarInfo(string filePath, string nombre_usuario) /// Guarda ordenes TXT, parametro nombre_usuario se usa para generar un TAG de quien creo la orden
   
       {

         string saveUser = nombre_usuario; 
        string separador = "#";
        string opcion = "D";

        try
        {
            if (File.Exists(filePath))
            {
                List<string> existingOrders = File.ReadAllLines(filePath).ToList();

                using (StreamWriter writer = new StreamWriter(filePath, true)) /// MODO APPEND
                {
                    for (int i = 0; i < lista_id.Count; i++)
                    {
                        string orderData = $"{lista_id[i]},{lista_nombre_articulos[i]},{lista_precios[i]},{lista_fechas_creacion[i]},{saveUser[i]}";
                        if (!existingOrders.Contains(orderData))
                        {
                            string mensaje = opcion + separador + saveUser + separador + lista_id[i];  // concatenacion de mensaje hacia servidor
                            SocketClient.StartClient(mensaje);// llamado al socket para enviar audit log
                            writer.WriteLine(orderData);
                        }
                    }
                }
            }
            else
            {
                using (StreamWriter writer = new StreamWriter(filePath)) /// CREAR EL ARCHIVO POR PRIMERA VEZ
                {
                    for (int i = 0; i < lista_id.Count; i++)
                    {
                        writer.WriteLine("{0},{1},{2},{3},{4}", lista_id[i], lista_nombre_articulos[i], lista_precios[i], lista_fechas_creacion[i], saveUser[i]);
                        string mensaje = opcion + separador + saveUser + separador + lista_id[i];  // concatenacion de mensaje hacia servidor
                        SocketClient.StartClient(mensaje); // llamado al socket para enviar audit log
                    }
                }
            }

            Console.WriteLine("Órdenes guardadas en el archivo con éxito."); /// Manejo de excepciones
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error al guardar las órdenes en el archivo: " + ex.Message); /// Manejo de excepciones
        }
    }

    public void CargarInfoArchivo(string filePath, string user_name) /// Carga de informacion desde archivo txt
    {
        string loadUser = user_name; 
        string opcion = "G";
        string separador = "#";
        string mensaje = opcion + separador + loadUser;  // concatenacion de mensaje hacia servidor
        SocketClient.StartClient(mensaje); // llamado al socket para enviar audit log
        try
        {
            if (File.Exists(filePath)) // valida existencia de archivos
            {
                lista_id.Clear();
                lista_nombre_articulos.Clear();
                lista_precios.Clear();
                lista_fechas_creacion.Clear();
                lista_usuario.Clear();
                using (StreamReader reader = new StreamReader(filePath))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null) // Lee el archivo txt hasta que sea null
                    {
                        string[] orderData = line.Split(',');

                        if (orderData.Length == 5)
                        {
                            int id = int.Parse(orderData[0]);
                            string nombre = orderData[1];
                            double precio = double.Parse(orderData[2]);
                            DateTime fechaCreacion = DateTime.Parse(orderData[3]);
                            string tempUser = orderData[4];
                            lista_id.Add(id);
                            lista_nombre_articulos.Add(nombre);
                            lista_precios.Add(precio);
                            lista_fechas_creacion.Add(fechaCreacion);
                            lista_usuario.Add(loadUser);
                        }
                    }
                }
                Console.WriteLine("ID\tNombre\t\tPrecio\tFecha de creación"); // IMPRESION DE ORDENES
                for (int i = 0; i < lista_id.Count; i++)
                {
                    if (loadUser == lista_usuario[i]) /// Solo permite imprimir ordenes del usuario conectado
                    {
                        Console.WriteLine("{0}\t{1}\t\t{2}\t{3}", lista_id[i], lista_nombre_articulos[i], lista_precios[i], lista_fechas_creacion[i]);
                    }

                }
                Console.WriteLine("Órdenes cargadas desde el archivo con éxito.");
            }
            else
            {
                Console.WriteLine("El archivo no existe.");  /// Manejo de excepciones
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error al cargar las órdenes desde el archivo: " + ex.Message); /// Manejo de excepciones
        }
    }

}




