/* 
Se insertan las librerías necesarias para el programa, incluyendo:
- fstream para manejo de ficheros
- list para manejo de listas
- thread para manejo de hilos
- ctime para manejo de fechas y horas
- la clase Usuario para obtener sus atributos y métodos
*/

#include "ServerSocket.hpp"
#include "SocketException.hpp"
#include <string>
#include <iostream>
#include "Usuario.hpp"
#include <fstream>
#include <list>
#include <memory>
#include <algorithm>
#include <vector>
#include <sstream>
#include <thread>
#include <ctime>

const int kPort = 6336;
const std::string kIp = "127.0.0.1";

/*
Se cargan los usuarios registrados previamente. Para esto, se busca el archivo
"users_data.txt" y se lee linea por linea un string que contiene todos los datos
de cada usuario, de esta manera, usando un ciclo for se separa este string en 
los atributos de cada clase, y luego se inserta este nuevo usuario a una lista 
de usuarios, creada una vez se inicia el programa.
*/

void LoadUsers(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList)
{
    std::string loadUserName;
    std::string loadUserPassword;
    std::string loadUserPhone;
    int loadUserID;
    bool loadUserStatus = false;
    std::string file_path = "users_data.txt";
    auto file = std::ifstream();
    file.open(file_path);
    std::string line;
    std::string separator = "#";
    while (std::getline(file, line))
    {
        for(int i = 0; i <= 3; i++)
        {
            std::string token = line.substr(0, line.find(separator));
            if (i == 0)
            {
                loadUserID = stoi(token);
            }
            else if (i == 1)
            {
                loadUserName = token;
            }
            else if (i == 2)
            {
                loadUserPassword = token;
            }
            else
            {
                loadUserPhone = token;
            }
            line.erase(0, line.find(separator) + separator.length());
        }
        std::shared_ptr<Usuario> loadUser = nullptr;
        loadUser = std::make_shared<Usuario>(loadUserName, loadUserPassword, loadUserPhone, loadUserID, loadUserStatus);
        usersList->push_back(loadUser);
    }
    file.close();
}

/*
Se crea una funcion que valida si el usuario que se desea crear ya existe.
Para esto, verifica si el ID ya existe o no, y devuelve un resultado a la
funcion AddNewuser().
*/

bool ValidateNewUser(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList, int id)
{
    bool result = true;
    for (auto i = usersList->begin(); i != usersList->end(); i++)
    {
        if (i->get()->GetUserID() == id)
        {
            result = false;
            break;
        }
    }
    return result;
}

/*
Recibe el string de datos de la manera A#ID#NOMBRE#CONTRASENA#TELEFONO
y lo separa en los atributos de la clase, siempre manteniendo su userStatus
como false, es decir, no conectado. Si la funcion ValidateNewUser() le 
indica que el usario ya existe, no crea al mismo y retorna un mensaje de
error, y si es posible crearlo, lo hace y retorna un mensaje de confirmacion.
*/
std::string AddNewUser(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList, std::string data)
{
    std::string separator = "#";
    std::string newUserName;
    std::string newUserPassword;
    std::string newUserPhone;
    std::string newUserID;
    bool newUserStatus = false;
    std::string result;
    for(int i = 0; i <= 3; i++)
    {
        std::string token = data.substr(0, data.find(separator));
        if (i == 0)
        {
            newUserID = token;
        }
        else if (i == 1)
        {
            newUserName = token;
        }
        else if (i == 2)
        {
            newUserPassword = token;
        }
        else
        {
            newUserPhone = token;
        }
        data.erase(0, data.find(separator) + separator.length());
    }
    std::shared_ptr<Usuario> newUser = nullptr;
    if (ValidateNewUser(usersList, stoi(newUserID)) == true)
    {
        std::string file_path = "users_data.txt";
        auto file = std::ofstream();
        file.open(file_path, std::ios::app);
        file << newUserID <<"#"<< newUserName << "#" << newUserPassword << "#" << newUserPhone << std::endl;
        file.close();
        newUser = std::make_shared<Usuario>(newUserName, newUserPassword, newUserPhone, stoi(newUserID), newUserStatus);
        usersList->push_back(newUser);
        file_path = "id_logs/logs_"+newUserID+".txt";
        file.open(file_path, std::ios::ate);
        time_t now = time(0);
        std::string dt = ctime(&now);
        file <<dt+"Creacion de usuario."<<std::endl << std::endl;
        file.close();
        result = "El usuario ha sido agregado exitosamente.";
        return result;
    } 
    else
    {
        result = "Este usuario ya existe.";
        return result;
    }
}

/*
Se crea una funcion que valida si el usuario que se desea conecta ya lo esta.
Para esto, verifica si el atributo userStatus de cada usuario se encuentra como true
y devuelve un resultado a la funcion LogIn().
*/

bool ValidateLogIn(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList, int id, std::string password)
{
    bool result = true;
    for (auto i = usersList->begin(); i != usersList->end(); i++)
    {
        if (i->get()->GetUserID() == id && i->get()->GetUserStatus() == true)
        {
            result = false;
            break;
        }
        else if (i->get()->GetUserID() == id && i->get()->GetUserPassword() == password && i->get()->GetUserStatus() == false)
        {
            i->get()->SetUserStatus(true);
            result = true;
            break;
        }
        else
        {
            result = false;
        }
    }
    return result;
}

/*
Recibe el string de datos de la manera B#ID#CONTRASENA y verifica si los
credenciales son correctos para iniciar sesion, siempre y cuando al funcion
ValidateLogIn() le indique que el usuario no se encuentra conectado.
*/

std::string LogIn(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList, std::string data)
{
    std::string separator = "#";
    std::string logInID;
    std::string logInPassword;
    std::string result;
    for(int i = 0; i <= 1; i++)
    {
        std::string token = data.substr(0, data.find(separator));
        if (i == 0)
        {
            logInID = token;
        }
        else
        {
            logInPassword = token;
        }
        data.erase(0, data.find(separator) + separator.length());
    }
    if (ValidateLogIn(usersList, stoi(logInID), logInPassword) == true)
    {
        //bool found = false;
        //for (auto i = usersList->begin(); i != usersList->end(); i++)
        //{
        //    if (i->get()->GetUserID() == stoi(logInID))
        //    {
        //        found = true;
        //        i->get()->SetUserStatus(true);
        std::string file_path = "id_logs/logs_"+logInID+".txt";
        auto file = std::ofstream();
        file.open(file_path, std::ios::app);
        time_t now = time(0);
        std::string dt = ctime(&now);
        file <<dt+"Inicio de sesion de usuario."<<std::endl << std::endl;
        file.close();
        result = "Inicio de sesion exitoso.";
        //    }
        //}
    } 
    else
    {
        result = "Este usuario ya se encuentra activo o no existe.";
    }
    return result;
}

/*
Recibe el string de datos de la manera C#ID# e indica que el usario 
pasa de estar activo a inactivo.
*/

std::string LogOut(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList, std::string data)
{
    std::string separator = "#";
    std::string logOutID;
    logOutID = data;
    std::string result;
    for (auto i = usersList->begin(); i != usersList->end(); i++)
    {
        if (i->get()->GetUserID() == stoi(logOutID))
        {
            i->get()->SetUserStatus(false);
        }
    }
    std::string file_path = "id_logs/logs_" + logOutID + ".txt";
    auto file = std::ofstream();
    file.open(file_path, std::ios::app);
    time_t now = time(0);
    std::string dt = ctime(&now);
    file <<dt+"Cierre de sesion de usuario."<<std::endl << std::endl;
    file.close();
    result = "Cierre de sesion exitoso.";
    return result;
}

/*
Recibe el string de datos de la manera D#ID#ORDEN e indica que el usario 
ha realizado la accion de agregar una orden, tomando como 
referencia el ID de la orden.
*/

std::string AddOrder(std::string data) 
{
    std::string separator = "#";
    std::string userID;
    std::string  orderID;
    std::string result; 
    for(int i = 0; i <= 1; i++)
    {
        std::string token = data.substr(0, data.find(separator));
        if (i == 0)
        {
            userID = token;
        }
        else
        {
            orderID = token;
        }
        data.erase(0, data.find(separator) + separator.length());
    }
    std::string file_path = "id_logs/logs_" + userID + ".txt";
    auto file = std::ofstream();
    file.open(file_path, std::ios::app);
    time_t now = time(0);
    std::string dt = ctime(&now);
    file <<dt+"Orden agregada: " + orderID <<std::endl << std::endl;
    file.close();
    result = "Accion agregada al servidor.";
    return result;
}

/*
Recibe el string de datos de la manera E#ID#ORDEN e indica que el usario 
ha realizado la accion de eliminar una orden, tomando como 
referencia el ID de la orden.
*/

std::string DeleteOrder(std::string data) 
{
    std::string separator = "#";
    std::string userID;
    std::string  orderID;
    std::string result; 
    for(int i = 0; i <= 1; i++)
    {
        std::string token = data.substr(0, data.find(separator));
        if (i == 0)
        {
            userID = token;
        }
        else
        {
            orderID = token;
        }
        data.erase(0, data.find(separator) + separator.length());
    }
    std::string file_path = "id_logs/logs_" + userID + ".txt";
    auto file = std::ofstream();
    file.open(file_path, std::ios::app);
    time_t now = time(0);
    std::string dt = ctime(&now);
    file <<dt+"Orden eliminada: " + orderID <<std::endl << std::endl;
    file.close();
    result = "Accion agregada al servidor.";
    return result;
}

/*
Recibe el string de datos de la manera F#ID#ORDEN e indica que el usario 
ha realizado la accion de consultar una orden, tomando como 
referencia el ID de la orden.
*/

std::string SeeOrder(std::string data) 
{
    std::string separator = "#";
    std::string userID;
    std::string  orderID;
    std::string result;
    for(int i = 0; i <= 1; i++)
    {
        std::string token = data.substr(0, data.find(separator));
        if (i == 0)
        {
            userID = token;
        }
        else
        {
            orderID = token;
        }
        data.erase(0, data.find(separator) + separator.length());
    }
    std::string file_path = "id_logs/logs_" + userID + ".txt";
    auto file = std::ofstream();
    file.open(file_path, std::ios::app);
    time_t now = time(0);
    std::string dt = ctime(&now);
    file <<dt+"Orden consultada: " + orderID <<std::endl << std::endl;
    file.close();
    result = "Accion agregada al servidor.";
    return result;
}

/*
Recibe el string de datos de la manera G#ID e indica que el usario 
ha realizado la accion de consultar todas las ordenes.
*/

std::string SeeAllOrders(std::string data)
{
    std::string separator = "#";
    std::string userID;
    userID = data;
    std::string result;
    std::string file_path = "id_logs/logs_" + userID + ".txt";
    auto file = std::ofstream();
    file.open(file_path, std::ios::app);
    time_t now = time(0);
    std::string dt = ctime(&now);
    file <<dt+"Todas las ordenes fueron consultadas."<<std::endl << std::endl;
    file.close();
    result = "Accion agregada al servidor.";
    return result;
}

/*
Funcion que procesa las acciones que vienen desde la aplicacion, 
manteniendo el orden establecido previamente.
*/

std::string ProcessRequest(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList, std::string rawData)
{
    std::string message;
    if (rawData[0] == 'A')
    {
        rawData= rawData.erase(0, 2);
        message = AddNewUser(usersList, rawData);
    }
    else if (rawData[0] == 'B')
    {
        rawData= rawData.erase(0, 2);
        message = LogIn(usersList, rawData);
    }
    else if (rawData[0] == 'C')
    {
        rawData= rawData.erase(0, 2);
        message = LogOut(usersList, rawData);
    }
    else if (rawData[0] == 'D')
    {
        rawData= rawData.erase(0, 2);
        message = AddOrder(rawData);
    }
    else if (rawData[0] == 'E')
    {
        rawData= rawData.erase(0, 2);
        message = DeleteOrder(rawData);
    }
    else if (rawData[0] == 'F')
    {
        rawData= rawData.erase(0, 2);
        message = SeeOrder(rawData);
    }
    else if (rawData[0] == 'G')
    {
        rawData= rawData.erase(0, 2);
        message = SeeAllOrders(rawData);
    }
    else
    {
        std::cout<<"La solicitud no pudo ser procesada"<<std::endl;
    }
    return message;
}

/*
Funcion que, desde el servidor, muestra todos los usuarios
que se han agregado, esto mediante el fichero creado y la 
lista que se actualiza inmediatamente.
*/

void SeeAllUsers(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList)
{
    std::cout << "---- Usuarios Totales ----" << std::endl;
    int user = 1;
    for (auto i = usersList->begin(); i != usersList->end(); i++)
    {
        std::cout << "Usuario "<< user << std::endl;
        std::cout << "ID: " << i->get()->GetUserID() << std::endl;
        std::cout << "Nombre: " << i->get()->GetUserName() << std::endl;
        std::cout << "Telefono: " << i->get()->GetUserPhone() << std::endl;
        user++;
    }
}

/*
Funcion que, desde el servidor, muestra todos los usuarios
que se encuentran conectados, esto mediante la lista de usuarios y
el atributo de userStatus de la clase.
*/

void SeeOnlineUsers(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList)
{
    std::cout << "---- Usuarios Activos ----" << std::endl;
    int user = 1;
    for (auto i = usersList->begin(); i != usersList->end(); i++)
    {
        if (i->get()->GetUserStatus() == true)
        {
            std::cout << "Usuario "<< user << std::endl;
            std::cout << "ID: " << i->get()->GetUserID() << std::endl;
            std::cout << "Nombre: " << i->get()->GetUserName() << std::endl;
            std::cout << "Telefono: " << i->get()->GetUserPhone() << std::endl;
            user++;
        }
    }
}

/*
Funcion que, desde el servidor, permite ver las acciones que ha 
realizado un determinado usuario, tomando como referencia el ID.
*/

void ConsultLogs(std::string data)
{
    std::list <std::string> actionsList;
    std::string userID;
    userID = data;
    std::string file_path = "id_logs/logs_" + userID + ".txt";
    auto file = std::ifstream();
    file.open(file_path);
    std::string line;
    while (std::getline(file, line))
    {
        actionsList.push_back(line);

    }
    file.close();
    auto i = actionsList.begin();
    while(i != actionsList.end())
    {
        std::cout<<*i++<<std::endl;
    }
}

/*
Esta funcion se ejecuta en un hilo, para que desde el servidor
se puedan ejecutar las acciones propias.
*/

void Menu(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList)
{
    int option;
    bool loop = true;
    std::string data;
    while (loop == true) 
    {
        std::cout << "-------- Menu --------" << std::endl;
        std::cout << "1. Mostrar usuarios" << std::endl;
        std::cout << "2. Mostrar usuarios conectados" << std::endl;
        std::cout << "3. Consultar registros" << std::endl;
        std::cout << "4. Salir" << std::endl;
        std::cout << "Ingrese la opcion deseada: ";
        std::cin >> option;
        switch (option) 
        {
            case 1:
                SeeAllUsers(usersList);
                break;
            case 2:
                SeeOnlineUsers(usersList);
                break;
            case 3:
                std::cout<<"Ingrese el ID  a consultar: ";
                std::cin>>data;
                ConsultLogs(data);
                break;
            case 4:
                std::cout << "Saliendo del sistema..." << std::endl;
                loop = false;
                break;   
        }         
    }
}

/*
Esta funcion se ejecuta en un hilo, para que el socket establezca
la conexion y reciba los datos que los pasa a la funcion ProcessRequest().
*/

void SocketIsWorking(std::unique_ptr<std::list<std::shared_ptr<Usuario>>> &usersList)
{
    try
    {
        // Create the socket
        ServerSocket server{kPort};
        std::cout << "Server is running: " << server.get_addr_str()<< ":" << kPort <<std::endl;

        while (true)
        {
            std::cout << "Listening connection: " << server.get_addr_str()<< ":" << kPort <<std::endl;
            ServerSocket new_sock;
            server.Accept(new_sock);

            try
            {
                std::string incomingMessage;
                std::string outcomingMessage;
                // Receive the message
                new_sock >> incomingMessage;
                //incomingMessage = "DatosParaProbar";
                outcomingMessage = ProcessRequest(usersList, incomingMessage);
                std::cout << "Received message: " << incomingMessage << std::endl;
                // Send the message back
                new_sock << outcomingMessage;
            }
            catch (SocketException &)
            {
                std::cout << "Something is wrong: " << new_sock.get_addr_str() <<std::endl;
            }
        }
    }
    catch (SocketException &e)
    {
        std::cout << "Exception was caught:" << e.description() << "\nExiting.\n";
    }
}

/*
Funcion principal en donde se crea un UniquePointer a una lista de datos
de tipo Usuario, se cargan los usuarios registrados previamente y se definen
los dos hilos que van a correr de manera paralela.
*/

int main()
{
    auto usersList =  std::make_unique<std::list<std::shared_ptr<Usuario>>>();
    LoadUsers(usersList);
    auto actionThread = std::thread(SocketIsWorking, std::ref(usersList));
    auto menuThread = std::thread(Menu, std::ref(usersList));
    actionThread.join();
    menuThread.join();
    return 0;
}
