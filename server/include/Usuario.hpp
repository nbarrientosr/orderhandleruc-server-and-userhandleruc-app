#include <string>

#pragma once

class Usuario
{
    public:

        Usuario(std::string userName, std::string userPassword, std::string userPhone, int userID, bool userStatus) 
        {
            this->userName = userName;
            this->userPassword = userPassword;
            this->userPhone = userPhone;
            this->userID = userID;
            this->userStatus = userStatus;
        }

        std::string GetUserName()
        {
            return userName;
        }

        std::string GetUserPassword()
        {
            return userPassword;
        }

        std::string GetUserPhone()
        {
            return userPhone;
        }

        int GetUserID() 
        {
            return userID;
        }

        bool GetUserStatus() 
        {
            return userStatus;
        }

        void SetUserStatus(bool status)
        {
            this->userStatus = status;
        }

    private:
        std::string userName;
        std::string userPassword;
        std::string userPhone;
        int userID;
        bool userStatus;
};